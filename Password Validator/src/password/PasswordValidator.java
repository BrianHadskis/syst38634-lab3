package password;

/**
 * 
 * @author Brian Hadskis 000001494
 * 
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGITS = 2;
	
	public static boolean isValidLength(String password) {
		return (password != null && password.length() >= MIN_LENGTH);
	}
	
	public static boolean hasValidDigitCount(String password) {
		int digitCount = 0;
		for (int i = 0; i < password.length(); i++) {
			if (Character.isDigit(password.charAt(i)))
				digitCount++;
			if (digitCount >= MIN_DIGITS)
				return true;
		}
		return false;
	}
}
