package password;

/**
 * 
 * @author Brian Hadskis 000001494
 * 
 * Password Validator
 * Validates passwords using the following requirements
 *  - password has at least 8 characters
 *  - password contains at least two digits
 * 
 * Created using TDD
 * 
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	@Test
	public void testIsValidLength_Regular() {
		assertTrue(PasswordValidator.isValidLength("amazingpassword"));
	}
	
	@Test
	public void testIsValidLength_Exceptional() {
		assertFalse(PasswordValidator.isValidLength(null));
	}
	
	@Test
	public void testIsValidLength_BoundaryIn() {
		assertTrue(PasswordValidator.isValidLength("password"));
	}
	
	@Test
	public void testIsValidLength_BoundaryOut() {
		assertFalse(PasswordValidator.isValidLength("invalid"));
	}
	
	
	@Test
	public void testHasValidDigitCount_Regular() {
		assertTrue(PasswordValidator.hasValidDigitCount("pass12345"));
	}
	
	@Test
	public void testHasValidDigitCount_Exceptional() {
		assertFalse(PasswordValidator.hasValidDigitCount("nodigits"));
	}
	
	@Test
	public void testHasValidDigitCount_BoundaryIn() {
		assertTrue(PasswordValidator.hasValidDigitCount("pass12"));
	}
	
	@Test
	public void testHasValidDigitCount_BoundaryOut() {
		assertFalse(PasswordValidator.hasValidDigitCount("password9"));
	}
}
